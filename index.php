<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {

    print('Спасибо, результаты сохранены.');
  }
  include('forma.php');
  exit();
}

$errors = FALSE;

if(empty($_POST['name'])) { 
    print('Введите имя.');
    $errors = TRUE;
    }
if(empty($_POST['email'])) {
    print('Введите email.');
    $errors = TRUE;
}
else if(!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $_POST['email'])){
    print('Email введён некорректно.');
    $errors = TRUE;
}
if(empty($_POST['superpower'])) {
    print('Выберите суперсилы.');
    $errors = TRUE;
}
if(empty($_POST['biography'])) {
    print('Введите свою биографию.');
    $errors = TRUE;}
if(empty($_POST['check'])) {
    print('Ознакомьтесь с контрактом.');
    $errors = TRUE;
}

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

$name = $_POST['name'];
$email = $_POST['email'];
$year = $_POST['year'];
$gender = $_POST['gender'];
$limbs = $_POST['limbs'];
$biography = $_POST['biography'];

$username = 'u20622';
$password = '2079517';

$dsn = 'mysql:host=localhost;dbname=u20622';
$pdo = new PDO($dsn, $username, $password, array(PDO::ATTR_PERSISTENT => true));

$sql = "insert into info (name, email, birth, gender, limbs, biography) values (:name, :email, :year, :gender, :limbs, :biography)";
$query = $pdo->prepare($sql);
$query->execute(['name' => $name, 'email' => $email, 'year' => $year, 'gender' => $gender, 'limbs' => $limbs, 'biography' => $biography]);

$id = $pdo->lastInsertId();

foreach ($_POST['superpower'] as $superpower){
    $que = $pdo->prepare('SELECT id_power FROM superpower where name_power = :superpower');
    $que->execute(['superpower' => $superpower]);
    $quer = $pdo->prepare('INSERT INTO connection (id, id_power) VALUES (:id, :id_power)');
    $quer->execute(['id' => $id, 'id_power' => $que->fetch(PDO::FETCH_LAZY)]);
}

header('Location: ?save=1');

?>
